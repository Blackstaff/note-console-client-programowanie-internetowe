/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package note_client;

import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Mateusz Czarnecki
 */
public class NoteHandlerTest {
    
    public NoteHandlerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getNoteList method, of class NoteHandler.
     */
    @Test
    public void testGetNoteList() throws Exception {
        System.out.println("getNoteList");
        NoteHandler instance = new NoteHandler();
        String result = instance.getNoteList();
        assertNotNull(result);
    }

    /**
     * Test of getNote method, of class NoteHandler.
     */
    @Test
    public void testGetNote() throws Exception {
        System.out.println("getNote");
        int noteId = 0;
        NoteHandler instance = new NoteHandler();
        String expResult = "";
        String result = instance.getNote(noteId);
        assertNotNull(result);
    }

    /**
     * Test of deleteNote method, of class NoteHandler.
     */
    @Test
    public void testDeleteNote() throws Exception {
        System.out.println("deleteNote");
        int noteId = 1;
        NoteHandler instance = new NoteHandler();
        String expResult = "Usunieto notatke " + noteId;
        String result = instance.deleteNote(noteId);
        assertEquals(expResult, result);
    }

    /**
     * Test of createNote method, of class NoteHandler.
     */
    @Test
    public void testCreateNote() throws Exception {
        System.out.println("createNote");
        JsonObject noteJson = new JsonObject();
        noteJson.addProperty("title", "Tytuł");
        noteJson.addProperty("content", "Treść");
        noteJson.addProperty("category_id", 0);
        noteJson.addProperty("tag", "");
        NoteHandler instance = new NoteHandler();
        String expResult = "Dodano nowa notatke";
        String result = instance.createNote(noteJson);
        assertEquals(expResult, result);
    }

    /**
     * Test of editNote method, of class NoteHandler.
     */
    @Test
    public void testEditNote() throws Exception {
        System.out.println("editNote");
        JsonObject noteJson = new JsonObject();
        noteJson.addProperty("title", "zmodyfikowany Tytuł");
        noteJson.addProperty("content", "zmodyfikowana Treść");
        noteJson.addProperty("category_id", 0);
        noteJson.addProperty("tag", "zmodyfikowany");
        int noteId = 0;
        NoteHandler instance = new NoteHandler();
        String expResult = "Zmodyfikowano notatke " + noteId;
        String result = instance.editNote(noteJson, noteId);
        assertEquals(expResult, result);
    }
    
}
