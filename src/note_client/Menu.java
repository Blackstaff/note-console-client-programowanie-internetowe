/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package note_client;

import asg.cliche.Command;
import asg.cliche.Param;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateusz Czarnecki
 */
public class Menu {

    public Menu() {

    }

    public int showMainMenu() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("[1] Pokaż listę notatek");
        System.out.println("[2] Pokaż listę kategorii");
        System.out.println("[3] Utwórz nową notatkę");
        System.out.println("[4] Utwórz nową kategorię");

        int option = scanner.nextInt();

        return option;
    }

    @Command(description="Pokazuje listę wszystkich notatek")
    public String showNoteList() {
        NoteHandler nh = new NoteHandler();
        String noteList = null;
        try {
            noteList = nh.getNoteList();
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return noteList;
    }

    @Command(description="Pokazuje szczegóły notatki o podanym id")
    public String readNote(
            @Param(name="noteId", description="Numer id notatki")
            int noteId) {
        NoteHandler nh = new NoteHandler();
        String note = null;
        try {
            note = nh.getNote(noteId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return note;
    }

    @Command(description="Usuwa notatkę o podanym id")
    public String deleteNote(
            @Param(name="noteId", description="Numer id notatki")
            int noteId) {
        NoteHandler nh = new NoteHandler();
        String message = null;
        try {
            message = nh.deleteNote(noteId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description="Edytuje notatkę o podanym id")
    public String editNote(
            @Param(name="noteId", description="Numer id notatki")
            int noteId) {
        NoteHandler nh = new NoteHandler();
        String message = null;
        try {
            nh.getNote(noteId);
            JsonObject note = showCreateNoteMenu();
            note.addProperty("note_id", noteId);
            message = nh.editNote(note, noteId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description="Tworzy nową notatkę")
    public String createNote() throws IOException {
        NoteHandler nh = new NoteHandler();
        JsonObject note = showCreateNoteMenu();
        String message = null;
        try {
            message = nh.createNote(note);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description="Pokazuje notatki z kategorii o podanym id")
    public String showNotesFromCategory(
            @Param(name="categoryId", description="Numer id kategorii")
            int categoryId) {
        NoteHandler nh = new NoteHandler();
        String noteList = null;
        try {
            noteList = nh.getNotesFromCategory(categoryId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return noteList;        
    }

    @Command(description="Pokazuje notatki o podanym tagu")
    public String showNotesFromTag(
            @Param(name = "tagName", description = "Nazwa tagu")
            String tagName) {
        NoteHandler nh = new NoteHandler();
        String noteList = null;
        try {
            noteList = nh.getNotesFromTag(tagName);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return noteList;     
    }

    @Command(description = "Pokazuje listę kategorii")
    public String showCategoryList() {
        CategoryHandler ch = new CategoryHandler();
        String categoryList = null;
        try {
            categoryList = ch.getCategoryList();
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "Id category_name\n" + categoryList;
    }

    @Command(description = "Usuwa kategorię o podanym id, nie można usunąć"
            + " kategorii o id 0")
    public String deleteCategory(
            @Param(name="categoryId", description="Numer id kategorii")
            int categoryId) {
        CategoryHandler ch = new CategoryHandler();
        String message = null;
        try {
            message = ch.deleteCategory(categoryId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description = "Edytuje kategorię o podanym id")
    public String editCategory(
            @Param(name="categoryId", description="Numer id kategorii")
            int categoryId) throws IOException {
        CategoryHandler ch = new CategoryHandler();
        System.out.println("Podaj nową nazwę dla kategori i"
                + ch.getCategoryName(categoryId) + ": \n");
        Scanner scanner = new Scanner(System.in);
        JsonObject category = new JsonObject();
        category.addProperty("category_name", scanner.nextLine());

        String message = null;
        try {
            message = ch.editCategory(category, categoryId);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description = "Tworzy nową kategorię")
    public String createCategory() {
        CategoryHandler ch = new CategoryHandler();

        System.out.println("Podaj nazwę nowej kategorii: \n");
        Scanner scanner = new Scanner(System.in);
        JsonObject category = new JsonObject();
        category.addProperty("category_name", scanner.nextLine());

        String message = null;
        try {
            message = ch.createCategory(category);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return message;
    }

    @Command(description = "Pokazuje listę tagów")
    public String showTagList() {
        TagHandler th = new TagHandler();
        String tagList = null;
        try {
            tagList = th.getTagList();
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        return tagList;
    }

    private JsonObject showCreateNoteMenu() throws IOException {
        Scanner scanner = new Scanner(System.in);
        JsonObject noteJson = new JsonObject();

        System.out.println("Podaj tytuł: \n");
        noteJson.addProperty("title", scanner.nextLine());
        System.out.println("Wybierz kategorię (podaj id): \n");

        CategoryHandler ch = new CategoryHandler();

        System.out.println(ch.getCategoryList());
        noteJson.addProperty("category_id", scanner.nextInt());

        System.out.println("Podaj tagi (kolejne oddzielając spacjami): \n");
        String tagString = scanner.nextLine();
        tagString = scanner.nextLine();
        StringTokenizer strTok = new StringTokenizer(tagString, " ");
        String[] tagArray = new String[strTok.countTokens()];
        int i = 0;
        while (strTok.hasMoreTokens()) {
            tagArray[i] = strTok.nextToken();
            i++;
        }
        noteJson.add("tags", new JsonParser().parse(new Gson().toJson(tagArray)));

        System.out.println("Podaj treść: \n");

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        StringBuilder sb = new StringBuilder();
        try {
            while ((line = in.readLine()) != null && line.length() != 0) {
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }

        noteJson.addProperty("content", sb.toString());

        return noteJson;
    }

}
