/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package note_client;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 *
 * @author Mateusz Czarnecki
 */
public class Requester {

    private String requestMethod;
    private JsonObject json;

    public Requester() {

    }

    public Requester(String requestMethod) {
        this.requestMethod = requestMethod;
    }
    
    public Requester(String requestMethod, JsonObject json) {
        this.requestMethod = requestMethod;
        this.json = json;
    }

    public JsonElement makeRequest(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(requestMethod);
        if (requestMethod.equals("POST") || requestMethod.equals("PUT")) {
            makePOSTRequest(connection);
        }
        int responseCode = connection.getResponseCode();

        if (responseCode == HttpURLConnection.HTTP_OK) {
            InputStream inputStream = connection.getInputStream();
            String response = getResponse(inputStream);

            JsonParser jsonParser = new JsonParser();
            connection.disconnect();
            return jsonParser.parse(response);
        }

        connection.disconnect();
        return null;
    }

    private String getResponse(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader(inputStream));
        StringBuilder jsonString = new StringBuilder();

        int code;
        while ((code = in.read()) != -1) {
            jsonString.append((char) code);
        }

        return jsonString.toString();
    }

    private void makePOSTRequest(HttpURLConnection connection) throws IOException {
        Gson gson = new Gson();
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.connect();
        try (DataOutputStream out = new DataOutputStream(connection.getOutputStream())) {
            //out.writeUTF(URLEncoder.encode(json.toString(), "UTF-8"));
            out.write(json.toString().getBytes());
            out.flush();
        }
    }

}
