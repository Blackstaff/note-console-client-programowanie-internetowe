/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package note_client;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Mateusz Czarnecki
 */
public class CategoryHandler {

    public CategoryHandler() {

    }

    public String getCategoryList() throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category";
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonArray json = (JsonArray) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        String categoryList = parseJsonToCategoryListString(json);
        return categoryList;
    }

    private String parseJsonToCategoryListString(JsonArray json) {
        StringBuilder categoryList = new StringBuilder();
        for (int i = 0; i < json.size(); i++) {
            JsonObject categoryJson = (JsonObject) json.get(i);
            String categoryString = categoryJson.getAsJsonPrimitive("category_id").getAsString()
                    + " " + categoryJson.getAsJsonPrimitive("category_name").getAsString();
            categoryList.append(categoryString);
            categoryList.append("\n");
        }

        return categoryList.toString();
    }

    public String getCategoryName(int categoryId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/"
                + Integer.toString(categoryId);
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonObject json = (JsonObject) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        return json.get("category_name").getAsString();
    }

    public String deleteCategory(int categoryId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/"
                + Integer.toString(categoryId);
        URL url = new URL(urlString);
        Requester requester = new Requester("DELETE");
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

    public String editCategory(JsonObject categoryJson, int categoryId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category/"
                + Integer.toString(categoryId);
        URL url = new URL(urlString);
        Requester requester = new Requester("PUT", categoryJson);
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

    public String createCategory(JsonObject categoryJson) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category";
        URL url = new URL(urlString);
        Requester requester = new Requester("POST", categoryJson);
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

}
