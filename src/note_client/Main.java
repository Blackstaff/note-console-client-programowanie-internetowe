package note_client;

import asg.cliche.ShellFactory;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mateusz Czarnecki
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        ShellFactory.createConsoleShell("note_client", "Note Client", new Menu())
                .commandLoop();
        
    }
    
}
