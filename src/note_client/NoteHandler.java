/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package note_client;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Mateusz Czarnecki
 */
public class NoteHandler {

    public NoteHandler() {

    }

    public String getNoteList() throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note";
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonArray json = (JsonArray) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        String noteList = parseJsonToNoteListString(json);
        return noteList;
    }

    private String parseJsonToNoteListString(JsonArray json) {
        StringBuilder noteList = new StringBuilder();
        for (int i = 0; i < json.size(); i++) {
            JsonObject noteJson = (JsonObject) json.get(i);
            String noteString = noteJson.getAsJsonPrimitive("title").getAsString();
            noteList.append(noteString);
            noteList.append("\n");
        }

        return noteList.toString();
    }

    public String getNote(int noteId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/"
                + Integer.toString(noteId);
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonObject json = (JsonObject) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        return parseJsonToNoteString(json);
    }

    private String parseJsonToNoteString(JsonObject json) throws IOException {
        StringBuilder note = new StringBuilder();
        note.append("Tytuł: ");
        note.append(json.get("title").getAsString());
        note.append("\n");

        note.append("Kategoria: ");
        int categoryId = json.get("category_id").getAsInt();
        CategoryHandler ch = new CategoryHandler();
        note.append(ch.getCategoryName(categoryId));
        note.append("\n");

        note.append("Etykiety: ");
        String[] tags = new Gson().fromJson(json.get("tags"), String[].class);
        for (String tag : tags) {
            note.append(tag);
            note.append(", ");
        }
        note.append("\n");

        note.append("Treść: \n");
        /* TODO improve content formatting */
        String content = json.get("content").getAsString();
        note.append(String.format("%32s", content).trim());
        note.append("\n");

        return note.toString();
    }

    public String deleteNote(int noteId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/"
                + Integer.toString(noteId);
        URL url = new URL(urlString);
        Requester requester = new Requester("DELETE");
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

    public String createNote(JsonObject noteJson) throws IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note";
        URL url = new URL(urlString);
        Requester requester = new Requester("POST", noteJson);
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

    public String editNote(JsonObject noteJson, int noteId) throws IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/note/"
                + Integer.toString(noteId);
        URL url = new URL(urlString);
        Requester requester = new Requester("PUT", noteJson);
        JsonObject json = (JsonObject) requester.makeRequest(url);

        return json.get("message").getAsString();
    }

    public String getNotesFromCategory(int categoryId) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/category"
                + "/" + categoryId + "/notes";
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonArray json = (JsonArray) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        return parseJsonToNoteListString(json);
    }

    public String getNotesFromTag(String tagName) throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/tag"
                + "/" + tagName + "/notes";
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonArray json = (JsonArray) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        return parseJsonToNoteListString(json);
    }

}
