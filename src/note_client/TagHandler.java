/*
 * The MIT License
 *
 * Copyright 2014 Mateusz Czarnecki.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package note_client;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author Mateusz Czarnecki
 */
public class TagHandler {
    
    public String getTagList() throws MalformedURLException, IOException {
        String urlString = "http://len.iem.pw.edu.pl/~czarnem1/apps/webservice/tag";
        URL url = new URL(urlString);
        Requester requester = new Requester("GET");
        JsonObject json = (JsonObject) requester.makeRequest(url);

        if (json.isJsonNull()) {
            return null;
        }

        String tagList = parseJsonToTagListString(json);
        return tagList;
    }
    
    private String parseJsonToTagListString(JsonObject json) {
        StringBuilder categoryList = new StringBuilder();
        String[] tags = new Gson().fromJson(json.get("tag_names"), String[].class);
        for (String tag : tags) {
            categoryList.append(tag);
            categoryList.append("\n");
        }

        return categoryList.toString();
    }
    
}
